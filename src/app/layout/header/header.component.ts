import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { User } from 'src/app/core/_models/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  is_loggedin:boolean=false;
  user= new User();
  data:any;
  constructor(

    ) { }

  ngOnInit() {

  }

}
