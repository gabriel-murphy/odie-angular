import { Injectable } from '@angular/core';
import { HttpHeaders} from '@angular/common/http';
import { APIService } from '../api.service';


@Injectable({
  providedIn: 'root'
})


export class EstimateService {

  constructor(private API: APIService) { }

  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  private getHeaders() {
    const token = localStorage.getItem('token');
    const headers = this.headers.append('Authorization', 'Bearer '+ token);
    return headers;
  }

  getEstimates(){
    console.log(this.getHeaders());
    return this.API.get('customer/getEstimates',this.getHeaders());
  }
  openEstimates(estimate){
    console.log(this.getHeaders());
    return this.API.post('customer/openEstimates',estimate,this.getHeaders());
  }


}
