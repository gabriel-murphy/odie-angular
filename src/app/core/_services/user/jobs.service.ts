import { Injectable } from '@angular/core';
import { HttpHeaders} from '@angular/common/http';
import { APIService } from '../api.service';


@Injectable({
  providedIn: 'root'
})


export class JobsService {

  constructor(private API: APIService) { }

  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  private getHeaders() {
    const token = localStorage.getItem('token');
    const headers = this.headers.append('Authorization', 'Bearer '+ token);
    return headers;
  }

  getUpcomingJobs(){
    return this.API.get('customer/getUpcomingJobs',this.getHeaders());
  }
  getCurrentJobs(){
    return this.API.get('customer/getCurrentJobs',this.getHeaders());
  }
  addGateCode(data){
    return this.API.post('customer/addGateCode',data, this.getHeaders());
  }
  uploadNOC(data){
    let token = localStorage.getItem('token');

    const headers = new HttpHeaders({
      'mimeType': 'multipart/form-data',
      'Authorization': 'Bearer '+ token
      });
      headers.append('Accept', 'application/json');

    return this.API.post('customer/uploadNOC',data, headers);
  }
}
