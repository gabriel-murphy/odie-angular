import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class APIService {

  private baseURL = environment.API_END_URL;
  public headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });
  constructor(private http: HttpClient) { }

  get(path:any, headers = this.headers): Observable<any> {
    return this.http.get(`${this.baseURL}${path}`, { headers: headers });
  }

  post(path:any, data:any, headers = this.headers): Observable<any> {
    return this.http.post<any>(`${this.baseURL}${path}`, data, { headers: headers });
  }

  put(path:any,data:any,headers = this.headers): Observable<any> {
    return this.http.put<any>(`${this.baseURL}${path}`,data, { headers: headers });
  }
  patch(path:any,headers = this.headers): Observable<any> {
    return this.http.patch<any>(`${this.baseURL}${path}`, { headers: headers });
  }

  delete(path:any) {
    return this.http.delete(`${this.baseURL}${path}`);
  }

}
