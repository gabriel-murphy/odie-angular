import { Injectable } from '@angular/core';
import { 
  Router,
  CanActivate,
  ActivatedRouteSnapshot
} from '@angular/router';

@Injectable()
export class CheckoutGuardService implements CanActivate {
  constructor(public router: Router) {}
  canActivate(route: ActivatedRouteSnapshot): boolean {

    console.log(this.router.url);

    if(this.router.url=='/cart'){
    return true;
}
    else{
        return false;
    }
  }
}