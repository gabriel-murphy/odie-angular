import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { APIService } from './api.service';

@Injectable({
  providedIn: 'root'
})


export class GuestService {

  constructor(private API: APIService) { }

  private baseURL = environment.API_END_URL;
  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  private getHeaders() {
    const token = localStorage.getItem('token');
    const headers = this.headers.append('Authorization', 'Bearer '+ token);
    return headers;
  }


  sendLoginCode(data){
    console.log('i am here');
    return this.API.post('auth/sendLoginCode',data, this.getHeaders());
  }



}
