import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToastService{
    constructor(private toaster:ToastrService){}
    successToaster(message:string,title:string){
        this.toaster.success(message,title,{  
            closeButton:true,
            progressBar:true,
            positionClass:'toast-bottom-right'
        });
    }
    errorToaster(message:string,title:string){
        this.toaster.error(message,title,{  
            closeButton:true,
            progressBar:true,
            positionClass:'toast-bottom-right'
        });
    }
}