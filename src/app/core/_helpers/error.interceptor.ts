import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthService } from '../_services';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private accountService: AuthService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                this.accountService.logoutFront();
            }
            console.log('error interceptor',err);
            let error  ;
            if(err.statusText){
                error=err.statusText;
            }
            if(err.error.message && err.error.errors==null){
                error=err.error.message;
            }else if(err.error && err.error.errors==null){
                error=err.error;
            }
            else{
                error=err.error.errors[0];
            }
            return throwError(error);
        }))
    }
}
