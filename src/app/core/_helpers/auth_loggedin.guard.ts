import { Injectable } from '@angular/core';
import { Router , CanActivate } from '@angular/router';
import { AuthService } from '../_services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthLoggedInGuard implements CanActivate {

  constructor(private _authService: AuthService, private _router: Router) { }

    canActivate(): boolean {
      console.log(this._authService.loggedIn());
      if (!this._authService.loggedIn()) {
            this._router.navigate([''])
            return false
        } else {
            // this._router.navigate(['/customer/dashboard'])
            return true
        }
    }
}
