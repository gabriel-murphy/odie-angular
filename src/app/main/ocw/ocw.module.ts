import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OcwRoutingModule } from './ocw-routing.module';
import { OcwComponent } from './ocw.component';


@NgModule({
  declarations: [
    OcwComponent
  ],
  imports: [
    CommonModule,
    OcwRoutingModule
  ]
})
export class OcwModule { }
