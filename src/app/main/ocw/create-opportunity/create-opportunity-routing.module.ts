import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateOpportunityComponent } from './create-opportunity.component';

const routes: Routes = [{ path: '', component: CreateOpportunityComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateOpportunityRoutingModule { }
