import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateOpportunityRoutingModule } from './create-opportunity-routing.module';
import { CreateOpportunityComponent } from './create-opportunity.component';


@NgModule({
  declarations: [
    CreateOpportunityComponent
  ],
  imports: [
    CommonModule,
    CreateOpportunityRoutingModule
  ]
})
export class CreateOpportunityModule { }
