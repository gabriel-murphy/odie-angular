import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OcwComponent } from './ocw.component';

describe('OcwComponent', () => {
  let component: OcwComponent;
  let fixture: ComponentFixture<OcwComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OcwComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OcwComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
