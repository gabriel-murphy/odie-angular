import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OcwComponent } from './ocw.component';

const routes: Routes = [
  { path: '', component: OcwComponent,children:[
    { path: '', loadChildren: () => import('./create-opportunity/create-opportunity.module').then(m => m.CreateOpportunityModule) },
    { path: 'create', loadChildren: () => import('./create-opportunity/create-opportunity.module').then(m => m.CreateOpportunityModule) }

  ] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OcwRoutingModule { }
