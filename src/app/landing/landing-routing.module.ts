import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FormsModule} from '@angular/forms';
import { LandingComponent } from './landing.component';

const routes: Routes = [
  { path: '', component: LandingComponent , children: [
    { path: '', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },

  ]},
];



@NgModule({
  imports: [RouterModule.forChild(routes) ,
    FormsModule
  ],
  exports: [RouterModule]
})
export class LandingRoutingModule { }
